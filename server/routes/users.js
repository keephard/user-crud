var express = require('express');
var router = express.Router();
const userModel = require('../models/user');

/* GET users listing. */
router.get('/', async function(request, response) {
  const users = await userModel.find({});

  try {
    response.send(users);
  } catch (error) {
    response.status(500).send(error);
  }
});

router.post("/", async (request, response) => {
  const user = new userModel(request.body);

  try {
    await user.save();
    response.send(user);
  } catch (error) {
    response.status(500).send(error);
  }
});

router.put("/:id", async (request, response) => {
  const userId = request.params.id;
  const data = request.body;
  
  try {
    await userModel.updateOne({"_id": userId}, {$set: data});
    response.send(data);
  } catch (error) {
    response.status(500).send(error);
  }
});

router.delete("/:id", async (request, response) => {
  const userId = request.params.id;
  
  try {
    await userModel.deleteOne({"_id": userId});
    response.send(userId);
  } catch (error) {
    response.status(500).send(error);
  }
});

module.exports = router;
