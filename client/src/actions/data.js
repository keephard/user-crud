import * as ActionTypes from '../utils/actionTypes'
import * as ApiServices from '../services/service'
import { notificationMessage } from '../utils/functions'

export const apiLoading = () => ({
  type: ActionTypes.API_LOADING
})

export const apiFailed = (error) => ({
  type: ActionTypes.API_FAILED,
  payload: error
})

export const getUsers = () => {
  return (dispatch, getState) => {
    dispatch(apiLoading())
    return ApiServices.getUsers()
      .then(response => {
        dispatch({
          type: ActionTypes.GET_USERS_SUCCESS,
          payload: response.data
        })
        return Promise.resolve(response.data)
      }).catch(error => {
        dispatch(apiFailed(error))
      })
  }
}

export const updateUser = (id, data) => {
  return (dispatch, getState) => {
    dispatch(apiLoading())
    return ApiServices.updateUser(id, data)
      .then(response => {
        dispatch({
          type: ActionTypes.UPDATE_USER_SUCCESS,
          payload: response.data
        })
        notificationMessage('success', 'User is updated successfully')
        return Promise.resolve(response.data)
      }).catch(error => {
        dispatch(apiFailed(error))
      })
  }
}

export const addUser = (data) => {
  return (dispatch, getState) => {
    dispatch(apiLoading())
    return ApiServices.addUser(data)
      .then(response => {
        dispatch({
          type: ActionTypes.ADD_USER_SUCCESS,
          payload: response.data
        })
        notificationMessage('success', 'User is added successfully')
        return Promise.resolve(response.data)
      }).catch(error => {
        dispatch(apiFailed(error))
      })
  }
}

export const deleteUser = (id) => {
  return (dispatch, getState) => {
    dispatch(apiLoading())
    return ApiServices.deleteUser(id)
      .then(response => {
        dispatch({
          type: ActionTypes.DELETE_USER_SUCCESS,
          payload: response.data
        })
        notificationMessage('success', 'User is deleted successfully')
        return Promise.resolve(response.data)
      }).catch(error => {
        dispatch(apiFailed(error))
      })
  }
}
