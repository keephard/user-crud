import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
} from '@material-ui/core'
import Dialog from '../common/dialog'

import { deleteUser } from '../../actions/data'

const useStyles = makeStyles(({ spacing, palette }) => ({
  modal: {
    width: 500
  },
}))

const DeleteDialog = ({ opened, handleClose, user, deleteUser }) => {
  const classes = useStyles()
  const handleDelete = () => {
    deleteUser(user._id).then(() => handleClose())
  }

  return (
    <Dialog
      opened={opened}
      onClose={handleClose}
    >
      <DialogTitle>Delete</DialogTitle>
      <DialogContent className={classes.modal}>
        Are you sure to delete "{user && user.name}" user?
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDelete} variant="contained" color="primary">
          Delete
        </Button>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteUser: (id) => {
      return dispatch(deleteUser(id))
    }
  }
}

export default connect(
  null,
  mapDispatchToProps
)(DeleteDialog)