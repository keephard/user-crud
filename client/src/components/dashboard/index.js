import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Button, Typography } from '@material-ui/core'

import List from './list'
import AddDialog from './addDialog'

import { getUsers } from '../../actions/data'

const useStyles = makeStyles(({ spacing }) => ({
  root: {
    maxWidth: 1200,
    padding: spacing(2),
    margin: '0 auto'
  },
  table: {
    marginTop: spacing(2),
    marginBottom: spacing(2)
  },
  addBtn: {
    width: 200,
  }
}))

function Dashboard({users, getUsers}) {
  const classes = useStyles()
  const [addDialogOpened, toggleAddDialog] = useState(false)

  useEffect(() => {
    if (users === null) {
      getUsers()
    }
  }, [])

  let data = users || []
  
  return (
    <div className={classes.root}>
      <Typography variant="h6">User List</Typography>
      <div className={classes.table}>
        <List data={data} />
      </div>
      <Box display="flex" justifyContent="flex-end">
        <Button className={classes.addBtn} variant="contained" color="primary" onClick={() => toggleAddDialog(true)}>+ Add</Button>
      </Box>

      <AddDialog opened={addDialogOpened} handleClose={() => toggleAddDialog(false)} />
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    loading: state.data.loading,
    users: state.data.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getUsers: () => {
      dispatch(getUsers())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)
