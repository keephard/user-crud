import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles'
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  TextField,
} from '@material-ui/core'
import Dialog from '../common/dialog'
import DatePicker from '../common/datepicker'
import Select from '../common/select'

import { addUser, updateUser } from '../../actions/data'
import { genders } from '../../utils/const'

const useStyles = makeStyles(({ spacing, palette }) => ({
  modal: {
    width: 500
  },
  field: {
    width: '100%',
    marginBottom: spacing(3)
  }
}))

const initialState = {
  name: '',
  date: moment(),
  age: '',
  gender: '',
}

const AddDialog = ({ opened, handleClose, user, addUser, updateUser }) => {
  const classes = useStyles()
  const [state, setState] = useState(initialState)
  useEffect(() => {
    if (user) {
      setState(user)
    }
  }, [user])
  const onClose = () => {
    setState(initialState)
    handleClose()
  }
  const onSaveHandler = () => {
    if (user) {
      updateUser(user._id, state).then(() => onClose())
    } else {
      addUser(state).then(() => onClose())
    }
  }
  const onChangeHandler = (type, value) => {
    setState({...state, [type]: value})
  }

  return (
    <Dialog
      opened={opened}
      onClose={onClose}
    >
      <DialogTitle>Add Dialog</DialogTitle>
      <DialogContent className={classes.modal}>
        <TextField
          className={classes.field}
          fullWidth
          label="Name"
          variant="outlined"
          value={state.name}
          onChange={(e) => onChangeHandler('name', e.target.value)}
        />
        <DatePicker
          className={classes.field}
          label="Date Application"
          value={state.date}
          onChange={(value) => onChangeHandler('date', value)}
        />
        <TextField
          className={classes.field}
          fullWidth
          type="number"
          label="Age"
          variant="outlined"
          value={state.age}
          onChange={(e) => onChangeHandler('age', e.target.value)}
        />
        <Select 
          className={classes.field}
          label="Gender"
          options={genders}
          value={state.gender}
          onChange={(value) => onChangeHandler('gender', value)}
        />
      </DialogContent>
      <DialogActions>
        <Button color="primary" variant="contained" onClick={onSaveHandler}>
          {user ? 'Save' : 'Add'}
        </Button>
        <Button color="primary" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (data) => {
      return dispatch(addUser(data))
    },
    updateUser: (id, data) => {
      return dispatch(updateUser(id, data))
    },
  }
}

export default connect(
  null,
  mapDispatchToProps
)(AddDialog)