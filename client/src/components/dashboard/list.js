import React, { useState } from 'react'
import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

import DeleteDialog from './deleteConfirm'
import AddDialog from './addDialog'

const useStyles = makeStyles(({ spacing }) => ({
  table: {
    minWidth: 650,
  },
  btn: {
    marginRight: spacing(1)
  },
  gender: {
    textTransform: 'capitalize'
  }
}))

const List = ({ data })  => {
  const classes = useStyles()
  const [deleteDialogOpened, toggleDeleteDialog] = useState(false)
  const [addDialogOpened, toggleAddDialog] = useState(false)
  const [selectedUser, setSelectedUser] = useState(null)
  
  return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>No</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Date</TableCell>
              <TableCell>Age</TableCell>
              <TableCell>Gender</TableCell>
              <TableCell/>
            </TableRow>
          </TableHead>
          <TableBody>
            {data && data.map((row, index) =>  {
              return (
                <TableRow key={index}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{moment(row.date).format('YYYY-MM-DD')}</TableCell>
                  <TableCell>{row.age}</TableCell>
                  <TableCell className={classes.gender}>{row.gender}</TableCell>
                  <TableCell>
                    <IconButton 
                      className={classes.btn}
                      aria-label="delete"
                      onClick={() => {
                        setSelectedUser(row)
                        toggleDeleteDialog(true)
                      }}
                    >
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                    <IconButton 
                      className={classes.btn}
                      aria-label="edit"
                      onClick={() => {
                        setSelectedUser(row)
                        toggleAddDialog(true)
                      }}
                    >
                      <EditIcon fontSize="small" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <DeleteDialog
        opened={deleteDialogOpened}
        handleClose={() => {
          toggleDeleteDialog(false)
          setSelectedUser(null)
        }}
        user={selectedUser}
      />
      <AddDialog
        opened={addDialogOpened}
        handleClose={() => {
          toggleAddDialog(false)
          setSelectedUser(null)
        }}
        user={selectedUser}
      />
    </>
  )
}

export default List
