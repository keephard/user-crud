import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { makeStyles } from '@material-ui/core/styles'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import store, { persistor } from './store/configureStore'
import Dashboard from './components/dashboard'

const useStyles = makeStyles(() => ({
  root: {
    height: '100vh'
  }
}))

function App() {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router>
            <Switch>
              <Route exact path='/' component={Dashboard} />
              <Redirect to='/' />
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    </div>
  )
}

export default App
