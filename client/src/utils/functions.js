import { toast } from 'react-toastify'

export const notificationMessage = (type, message) => {
  toast.configure({
    autoClose: 3000,
    draggable: false,
    position: 'top-center',
    hideProgressBar: true
  })

  toast[type](message)

  return `${type}, ${message}`
}
