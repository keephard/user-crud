import { combineReducers } from 'redux'

import data from './data'

const RootReducer = combineReducers({
  data,
})

export default RootReducer
