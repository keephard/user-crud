import _ from 'lodash'
import * as ActionTypes from '../utils/actionTypes'

const initialState = {
  loading: false,
  users: null
}

const reducer = (state = initialState, action) => {
  let newState = _.cloneDeep(state)

  switch (action.type) {
    case ActionTypes.API_LOADING:
      newState.loading = true
      return newState

    case ActionTypes.API_FAILED:
      newState.loading = false
      return newState

    case ActionTypes.GET_USERS_SUCCESS:
      newState.loading = false
      newState.users = action.payload
      return newState

    case ActionTypes.ADD_USER_SUCCESS: {
      newState.loading = false
      newState.users = [...newState.users, action.payload]
      return newState
    }

    case ActionTypes.UPDATE_USER_SUCCESS: {
      newState.loading = false
      newState.users = newState.users.map(user => user._id !== action.payload._id ? user : action.payload)
      return newState
    }

    case ActionTypes.DELETE_USER_SUCCESS: {
      newState.loading = false
      newState.users = newState.users.filter(user => user._id !== action.payload)
      return newState
    }

    default:
      return state
  }
}

export default reducer
