import { requestService } from './request'
import * as ApiUrls from './apiUrl'
import store from '../store/configureStore'

export const commonService = (request, token = true) => {
  let reqObj = {
    method: request.method,
    url: request.url
  }
  const state = store.getState();
  if (token && state.auth && state.auth.token) {
    reqObj = Object.assign(reqObj, { token: `Bearer ${state.auth.token}` })
  }
  if (request.data) {
    reqObj = Object.assign(reqObj, { data: request.data })
  }
  if (request.queryParams) {
    reqObj = Object.assign(reqObj, { queryParams: request.queryParams })
  }
  return requestService(reqObj)
    .then(response => {
      return response
    }).catch(error => {
      return Promise.reject(error.response.data)
    })
}

export const getUsers = () => {
  return commonService({
    method: 'GET',
    url: ApiUrls.USER_ENDPOINT
  })
}

export const updateUser = (id, data) => {
  return commonService({
    method: 'PUT',
    url: ApiUrls.USER_ENDPOINT + `/${id}`,
    data
  })
}

export const addUser = (data) => {
  return commonService({
    method: 'POST',
    url: ApiUrls.USER_ENDPOINT,
    data
  })
}

export const deleteUser = (id) => {
  return commonService({
    method: 'DELETE',
    url: ApiUrls.USER_ENDPOINT + `/${id}`,
  })
}
