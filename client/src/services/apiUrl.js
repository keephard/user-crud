/**
 * api base url
 * type { String }
 */
export const { REACT_APP_API_URL } = process.env

export const USER_ENDPOINT = `${REACT_APP_API_URL}/user`
